package com.example.vpp.adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.vpp.fragments.FragmentProfile
import com.example.vpp.fragments.FragmentUserInput

class VpFa(activity: AppCompatActivity):FragmentStateAdapter(activity) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
       return when(position){
           0 -> FragmentProfile()
           1 -> FragmentUserInput()
           else -> FragmentProfile()
       }
    }
}