package com.example.vpp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.example.vpp.adapters.VpFa
import kotlinx.android.synthetic.main.profile.*
import kotlinx.android.synthetic.main.user_input.*

class MainActivity : AppCompatActivity() {
    private lateinit var viewPager: ViewPager2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewPager = findViewById(R.id.viewPager)
        val viewPagerFragmentAdapter= VpFa(this)
        viewPager.adapter = viewPagerFragmentAdapter

        load.setOnClickListener(){
            loaddata()
        }
        saveBtn.setOnClickListener {
            saveData()
        }
    }
    private fun saveData(){
        val name = nameEt.text.toString()
        nameTv.text = name
        val surname = surnameEt.text.toString()
        surnameTv.text = surname
        val age = ageEt.text.toString()
        ageTv.text = age
        val url = urlEt.text.toString()

       val sharedPreferences = getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
       val editor = sharedPreferences.edit()
       editor.apply(){
           putString("NAME",name)
           putString("SURNAME",surname)
           putString("AGE",age)
           putString("URL",url)
       }.apply()
        Toast.makeText(this,"Data Saved",Toast.LENGTH_SHORT).show()
    }
    private fun loaddata(){
        val sharedPreferences = getSharedPreferences("sharedPrefs",Context.MODE_PRIVATE)
        val savedName = sharedPreferences.getString("NAME",null)
        val savedSurname = sharedPreferences.getString("SURNAME",null)
        val savedAge = sharedPreferences.getString("AGE",null)
        val savedUrl = sharedPreferences.getString("URL",null)
        nameTv.text = savedName
        surnameTv.text = savedSurname
        ageTv.text = savedAge
        Glide.with(this)
            .load(savedUrl)
            .into(urlIv)
    }
}